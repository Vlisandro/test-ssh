//Include api modules.
const express    = require('express');
const http    = require('http');
const config     = require('config');
const proxy = require('express-http-proxy');
const logger = require('../lib/logger')();
const expressWinston = require('express-winston');

//Define routes and events
const routes = require('./routes');
const events = require('./events.js');
const { proxyBuilder } = require('../lib/proxies');
const { errorHandler } = require('./middleware.js');

//Start Express-js.
const app    = express();
const server = http.createServer(app);

//Bind error handler middleware.
app.use(errorHandler);
const getProxies = (key) => (config.has(key) && config.get(key)) || [];

// Set the app port.
const port = config.get('port') || 3000;


// Log payloads
expressWinston.requestWhitelist.push('body');
expressWinston.responseWhitelist.push('body');

app.use('/health',routes.health);

// Build the proxy.
proxyBuilder({
    app,
    logger,
    proxy,
    services: getProxies('proxies'),
  });


//Define server "special" event to handle situations.
server.on('error',   events.onServerError);
process.on('SIGINT', ()=>events.onProcessKill(server));
process.on('SIGTERM',()=>events.onProcessKill(server));
process.on('unhandledRejection', events.onException);
process.on('uncaughtException',  (err)=>events.onException(err));

//Start listen mode.
app.listen(port,()=>events.onListen(port));