const logger = require('winston');
const {verify} = require('../../lib/cognito');
/**
 * Health controller.
 * @param {object} req request object.
 * @param {object} res response object.
 */
const health = async (req,res, next)=>{ 
  try {
    await verify(req.headers.token);
    logger.info({url: req.url, method: req.method, message:`Health request`});
    res.status(200).json({"health":"OK"});
  } catch (error) {
    next(error);
  }
}

module.exports = health