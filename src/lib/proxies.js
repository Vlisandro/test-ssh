
const proxyBuilder = ({ app, logger, proxy, services }) =>
  services.forEach((tunnel) =>
    tunnel.routes.forEach((route) => {
      logger.info(`Create endpoint ${route.method}-${tunnel.endpoint}${route.path}`);

      app[route.method](
        `${tunnel.endpoint}${route.path}`,
        proxy(tunnel.host, {
          proxyReqPathResolver: (req) => req.originalUrl.replace(tunnel.endpoint, ''),
        }),
      );
    }),
  );

/**
 * Export all available modules.
 */
module.exports = {
  proxyBuilder,
};
