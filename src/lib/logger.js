const winston = require('winston');

module.exports = () => {
  const myconsole = new winston.transports.Console();
  winston.add(myconsole);
  return winston;
}