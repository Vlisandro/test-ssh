
const axios   = require('axios');
const jwkToPem  = require('jwk-to-pem');
const jwt       = require('jsonwebtoken');
const config     = require('config');
const { transports } = require('winston');
const createError = require('http-errors');
const region = config.get('aws-region');
const UserPoolId = config.get('user-pool');


//Download jwsk.
const downloadJwk =async ()=>{

    const urlJwk = `https://cognito-idp.${region}.amazonaws.com/${UserPoolId}/.well-known/jwks.json`;
  
    const response = await axios.get(urlJwk);
  
    if (response && response.status === 200) 
     return response.data 
    else
     throw createError.BadGateway();
  
  }

//Verify token.
const verify = async (token)=>{

    //Download jwkt from aws.
    const data = await downloadJwk();
    
    let pems = {};
    const keys = data.keys; 

    for(let i = 0; i < keys.length; i++) {

        //Convert each key to PEM
        let key_id   = keys[i].kid;
        let modulus  = keys[i].n;
        let exponent = keys[i].e;
        let key_type = keys[i].kty;
        let jwk = { kty: key_type, n: modulus, e: exponent};
        let pem = jwkToPem(jwk);

        pems[key_id] = pem;

    }

    //validate the token
    let decodedJwt = jwt.decode(token, {complete: true});

    //If is not valid.
    if (!decodedJwt)
        throw createError.BadRequest({"error":"Not valid token"});

    let kid = decodedJwt.header.kid;
    let pem = pems[kid];
    
    if (!pem)
        throw createError.BadRequest({"error":"Not valid token"});

    return new Promise((resolve,reject)=>{
        jwt.verify(token, pem, (err, payload)=>{
        
            if(err)
                 reject(createError.BadRequest({"error":"Not valid token"}));
            else
                resolve(payload);
    
        });
    })    

  
}
  
module.exports = {
    verify
}