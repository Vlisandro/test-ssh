<img src="https://github.com/damiancipolat/node-bff/blob/master/doc/node.png?raw=true" width="150px" align="right" />

# Bff Service

### **Stack**:
- Node.js v12
- Docker.
- Test: mocha / chai / sinon / proxyquire / nyc.
- config: Para manejar diferentes archivos de configuración por cada entorno.
- express.js
- winston: Para logear en forma de json al stdout.
- node-fetch: Para realizar request.

### **Configuración**:
Dentro del directorio /config se encuentran los archivos json de cada entorno, por defecto se usa "default.json".

```console
{
  "server":{
    "port":8080,
    "killTimeout":100
  }
}
```

### **Instalación**:

- Ejecutar test:
```console
fintech@api:~$ npm install
```

### **Comandos**:

- Ejecutar test:
```console
fintech@api:~$ npm test
```
- Ejecutar revisión de cobertura de codigo:
```console
fintech@api:~$ npm run coverage
```
- Ejecutar servidor:
```console
fintech@api:~$ npm start
```

### **Manejo de rutas**:
Por cada config se setan las rutas de cada entorno para agregar una nueva ruta hay que sumarlo al pool de proxies:

```console
      "proxies": [
        {
          "host": "https://run.mocky.io",
          "endpoint": "/mocky",
          "routes": [
            { "method": "get", "path": "/v3/8cc51fbe-58b8-4b95-8fcc-f3eb544b7f11" }
          ]
        }
      ]
```

