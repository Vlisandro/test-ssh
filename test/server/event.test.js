const chai  = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');

const mockConsole = {
    info: sinon.fake(),
    error: sinon.fake()
}
const mockProcess = {
    exit: sinon.fake()
};
const mockServer = {
    close: callback => {
        callback()
    }
};
const { onListen, onProcessKill, onServerError , onException } = proxyquire('../../src/server/events', {
    'winston': mockConsole,
    'process': mockProcess
});

describe('Events Test', () => {

    let clock;
    beforeEach(()=>{
        clock = sinon.useFakeTimers();        
        mockConsole.info.resetHistory();
        mockConsole.error.resetHistory();
        mockProcess.exit.resetHistory();
    });

    afterEach(()=>{
        clock.restore();
    });

    it('onListen method logs at least one message', () => {
        onListen('1.1.1.1', 3000);
        chai.expect(mockConsole.info.callCount).to.be.greaterThan(0);
    });

    it('onServerError method logs at least one message', () => {
        onServerError();
        chai.expect(mockConsole.error.callCount).to.be.greaterThan(0);
        chai.expect(mockConsole.error.firstCall.args).to.be.eql([{message:`Server error`}]);
    });

    it('onException method logs at least one message', () => {
        onException({ error: 'My exception'} );
        chai.expect(mockConsole.error.callCount).to.be.greaterThan(0);
        chai.expect(mockConsole.error.firstCall.args).to.be.eql([
              "error",
               {
                "message": {
                  "error": "My exception"
                }
               }
             ]
      );
    });

    it('onProcessKill stops server listening',async () => {
        const mockServer = {
            close: sinon.fake()
        }

        const promise = onProcessKill(mockServer);        
        await promise;
        clock.tick(120000);
        chai.expect(mockServer.close.callCount).to.be.eql(1);
    });

    it('onProcessKill calls process exit once',async () => {
        const promise = onProcessKill(mockServer);        
        await promise;
        clock.tick(120000);
        chai.expect(mockProcess.exit.callCount).to.be.eql(1);
    });

    it('onProcessKill exit process with no errors',async () => {
        const promise = onProcessKill(mockServer);        
        await promise;
        clock.tick(120000);
        chai.expect(mockProcess.exit.getCall(0).args).to.be.eql([0]);
    });
})