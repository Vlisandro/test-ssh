const chai  = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const { logger } = require('express-winston');

const mockWinston = {
    transports: {
        Console: sinon.fake()
    },
    add: sinon.fake()
}
const {proxyBuilder} = require('../../src/lib/proxies');

describe('Proxy builder Test', () => {

    it('proxyBuilder functionallity', () => {
        const mockGet= sinon.stub().returns(true)
        const app = {
            get: mockGet,
            post: sinon.fake(),
            put: sinon.fake(),
            delete:sinon.fake(),
        } ;
        const mockInfo= sinon.stub().returns(true)
        const logger = {
            info: mockInfo,
        }
        const mockProxy=sinon.stub().returns(true);
        const proxy = mockProxy;
        const services= [
            {
              "host": "https://run.mocky.io",
              "endpoint": "/mocky",
              "routes": [
                { "method": "get", "path": "/v3/8cc51fbe-58b8-4b95-8fcc-f3eb544b7f11" }
              ]
            }
        ];
        const req = sinon.stub().returns(true)
        proxyBuilder({
            app,
            logger,
            proxy,
            services
        })
        chai.expect(mockInfo.callCount).to.eql(1);
        chai.expect(mockGet.callCount).to.eql(1);
        chai.expect(mockGet.firstCall.args).to.eql(['/mocky/v3/8cc51fbe-58b8-4b95-8fcc-f3eb544b7f11', true]);
        chai.expect(mockProxy.callCount).to.eql(1);
        chai.expect(mockProxy.firstCall.args.length).to.eql(2);
        chai.expect(mockProxy.firstCall.args[0]).to.eql('https://run.mocky.io');
        chai.expect(mockProxy.firstCall.args[1]).to.be.an('object')        
    });

})