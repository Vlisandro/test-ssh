FROM node:10.16.0-alpine
LABEL Maintainer="Lucas Llopis"
ENV APP_DIR microservice_bff
ENV TZ=America/Buenos_Aires
WORKDIR /usr/app/${APP_DIR}
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run test
RUN npm prune --production
RUN echo "hello"
EXPOSE 8080
CMD [ "npm" , "start" ]
